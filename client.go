package awin

import (
	"log"
	"time"

	"gitlab.com/bazzz/csv"
	"gitlab.com/bazzz/textparse"
)

const listURL = "https://productdata.awin.com/datafeed/list/apikey/"

type Client struct {
	apiKey string
}

func New(apiKey string) *Client {
	return &Client{apiKey: apiKey}
}

func (c *Client) Listings(lastUpdate time.Time) ([]Listing, error) {
	url := listURL + c.apiKey
	feedDocument, err := csv.New(url, ',', "")
	if err != nil {
		log.Println("could not load csv:", err)
	}

	feeds := make([]csv.Row, 0)
	for _, feed := range feedDocument.Rows() {
		if feed.Get("Membership Status") != "active" {
			continue
		}
		lastImportedString := feed.Get("Last Imported")
		lastImported, err := time.Parse("2006-01-02 15:04:05", lastImportedString)
		if err != nil {
			log.Println("could not get last imported date from feed:", err)
			continue
		}
		if lastImported.Before(lastUpdate) {
			continue
		}
		feeds = append(feeds, feed)
	}

	products := make([]Listing, 0)
	for _, feed := range feeds {
		url = feed.Get("URL")
		document, err := csv.New(url, ',', "gzip")
		if err != nil {
			log.Println("could not load csv:", err)
		}
		for _, line := range document.Rows() {
			product := Listing{
				GTIN:           textparse.Integer(line.Get("product_GTIN")),
				EAN:            textparse.IntegerSlice(line.Get("ean")),
				MerchantID:     textparse.Integer(line.Get("merchant_id")),
				MerchantName:   line.Get("merchant_name"),
				Name:           textparse.Clean(line.Get("product_name")),
				Brand:          textparse.Clean(line.Get("brand_name")),
				Link:           line.Get("aw_deep_link"),
				Image:          line.Get("aw_image_url"),
				CategoryName:   line.Get("category_name"),
				Category1:      line.Get("merchant_category"),
				Category2:      line.Get("merchant_product_second_category"),
				Category3:      line.Get("merchant_product_third_category"),
				Description:    textparse.Clean(line.Get("description")),
				Price:          textparse.Float(line.Get("search_price")),
				Type:           line.Get("product_type"),
				Colour:         line.Get("colour"),
				Dimensions:     line.Get("dimensions"),
				Specifications: line.Get("specifications"),
			}
			products = append(products, product)
		}
	}
	return products, nil
}
