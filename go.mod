module gitlab.com/bazzz/awin

go 1.20

require (
	gitlab.com/bazzz/csv v0.0.0-20231122135316-07be8808a7d8
	gitlab.com/bazzz/textparse v0.0.0-20240331144504-cd8fac84c8d8
)
