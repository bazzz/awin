package awin

// Listing represents a specific product of the product feed.
type Listing struct {
	GTIN           int
	EAN            []int
	MerchantID     int
	MerchantName   string
	Name           string
	Brand          string
	Link           string
	Image          string
	CategoryName   string
	Category1      string
	Category2      string
	Category3      string
	Description    string
	Price          float64
	Type           string
	Colour         string
	Dimensions     string
	Specifications string
}
